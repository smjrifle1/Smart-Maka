<?php

//load and connect to MySQL database stuff
require("config.php");

if (!empty($_POST)) {
  
    $username = mysql_real_escape_string($_POST['username']);
    $result = mysql_query("Select password from users where username = '$username'");
    if(!$result) {
         $response["success"] = 0;
        $response["message"] = "Database Error1. Please Try Again!";
        die(json_encode($response));
    }
   
    
    //This will be the variable to determine whether or not the user's information is correct.
    //we initialize it as false.
    
    $login_ok=false;
    //fetching all the rows from the query
    if (mysql_num_rows($result) > 0) {
        $row = mysql_fetch_row($result);
        //if we encrypted the password, we would unencrypt it here, but in our case we just
        //compare the two passwords
        if ($_POST['password'] == $row[0]) {
            $login_ok = true;
        }
    }
    else {
        $response["success"] = 0;
        $response["message"] = "No user found!";
        die(json_encode($response));
    }
    
    // If the user logged in successfully, then we send them to the private members-only page 
    // Otherwise, we display a login failed message and show the login form again 
    if ($login_ok) {
        $response["success"] = 1;
        $response["message"] = "Login successful!";
        die(json_encode($response));
    } else {
        $response["success"] = 0;
        $response["message"] = "Invalid credentials";
        die(json_encode($response));
    }
} else {
?>
		<h1>Login</h1> 
		<form action="login.php" method="post"> 
		    Username:<br /> 
		    <input type="text" name="username" placeholder="username" /> 
		    <br /><br /> 
		    Password:<br /> 
		    <input type="password" name="password" placeholder="password" value="" /> 
		    <br /><br /> 
		    <input type="submit" value="Login" /> 
		</form> 
		<a href="register.php">Register</a>
	<?php
}

?> 
