
#include <RFM12B.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(5, 6); // RX, TX

#define NETWORKID       212
#define NODEID            1        // network ID used for this unit
#define KEY "ABCDABCDABCDABCD"     ///
#define HUBNUMBER "1A"
#define EOL '#'

#define REQUESTACK     true        // whether to request ACKs for sent messages
#define ACK_TIME         50        // # of ms to wait for an ack
#define TXWAIT         100
#define REGDELAY       1000
#define ARD2MON_SERIAL_BAUD  9600   //The baud rate between arduino and serial monitor 
#define ARD2PI_SERIAL_BAUD  4800    // The baud rate between Arduino to raspberrypi

#define statusLED               9       // Status of RF tx/rx
#define regInterrupt            4

/* Project Maka */
char  input;

RFM12B radio;
char mode = ' ';      

char pinState = '0';

char nodeId = 2;      //node to send message to
char senderNodeId;  //The node id of the Sender

char recvBuf[7];    // RF recieve buffer
char sendBuf[7];  // RF send buffer
char serialBuf[7];

void initRadio(){
  Serial.begin(ARD2MON_SERIAL_BAUD);
  mySerial.begin(ARD2PI_SERIAL_BAUD);
  
  Serial.print("Initializing Radio..");
  radio.Initialize(NODEID, RF12_433MHZ, NETWORKID);
  radio.Encrypt((uint8_t*)KEY);
  Serial.println("Success");
 
}
void setup()
{
  pinMode(statusLED, OUTPUT);
  pinMode(regInterrupt,OUTPUT);
  digitalWrite(regInterrupt,LOW);
  initRadio();
  delay(1000);
  Serial.println("[System ready]");
}

void loop()
{
  boolean registering=false;
  long now;
  long then=0;
   now = millis();  
  if(registering==true){
      if((now-then)<REGDELAY)
      digitalWrite(regInterrupt,HIGH);
      else {
        now=0;
        then=0;
        digitalWrite(regInterrupt,LOW);
        registering=false;
      }
      then = now;
  }
if (mySerial.available() > 0) {
    input = mySerial.read();
   Serial.write(input);
    
    if (input == 'w' || input=='r'|| input == 'R')  // Write mode
      mode = input;
   
    else if(input == '1' || input == '0'|| input == '-1')  
     pinState = input;
     
    else if (input == '#' && mode !=' ') 
      sendMsg();

   
  }
  
  recvMsg();
  if(recvBuf[0]=='R'){        // A module is requesting for Registration
  sprintf(serialBuf, "%cd%cn%d%c", recvBuf[0],recvBuf[1],senderNodeId,EOL); // [R:N:I] (R is register code, N is the device name, I is the node id)
  mySerial.flush();
 digitalWrite(regInterrupt,HIGH);
  delay(REGDELAY);
  digitalWrite(regInterrupt,LOW);
 mySerial.print(serialBuf);
 Serial.println(serialBuf);
  recvBuf[0]=' '; 
  registering=true;
}
 else if(recvBuf[0]=='T'){   //A module is sending temperature data
   sprintf(serialBuf, "%c%c%c", recvBuf[0],recvBuf[1],recvBuf[2],EOL); // [R:N:I] (R is register code, N is the device name, I is the node id)
   mySerial.flush();
   mySerial.print(serialBuf);
   Serial.println(serialBuf); // also print in the console
   recvBuf[0]=' ';
 }
 
  
  
  
}

void sendMsg()
{
  char success = 0;
  long now = millis();
  sprintf(sendBuf, "%c:%c", mode, pinState);
  while(((millis()-now)<TXWAIT) && success==0){
  radio.Send(nodeId, sendBuf, 3, REQUESTACK);
  Serial.print("Request sent to node : ");
  Serial.println(nodeId);
  if(waitForAck()){
    mySerial.println('1');
    Serial.println("ACK ok");
    success=1;
  }
  else{
    mySerial.println('0');
    Serial.println("NO ACk reply");
  }
    }
  //mySerial.flush();
  Blink(statusLED, 5);
}


static bool waitForAck() {
  long now = millis();
  while (millis() - now <= ACK_TIME)
    if (radio.ACKReceived(nodeId))
      return true;
  return false;
}

void Blink(byte PIN, byte DELAY_MS)
{
  pinMode(PIN, OUTPUT);
  digitalWrite(PIN,HIGH);
  delay(DELAY_MS);
  digitalWrite(PIN,LOW);
}

void recvMsg(void){
if (radio.ReceiveComplete())
  {
    
    if (radio.CRCPass())
     {
       byte i;
      senderNodeId= radio.GetSender();
      Serial.print('[');Serial.print(radio.GetSender());Serial.print("] ");
      Serial.print("Recieved: ");
      for (i = 0; i < *radio.DataLen; i++){ 
        Serial.print((char)radio.Data[i]);
        recvBuf[i]=radio.Data[i];
      }
      recvBuf[i] = '\0';
      if (radio.ACKRequested())
      {
        radio.SendACK();
        Serial.print(" - ACK sent");
      }
    }
    else
    {
      Serial.print("BAD-CRC");

    }
    
    Serial.println();
  }
  else 
    return;
}
