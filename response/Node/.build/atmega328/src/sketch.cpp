#include <Arduino.h>
#include <RFM12B.h>
#include <EEPROM.h>
void initState();
void initRadio();
void setup(void);
void loop();
void sendMsg(char param1,char param2,char ender);
void recvMsg(void);
static bool waitForAck();
#line 1 "src/sketch.ino"

//#include <RFM12B.h>
//#include <EEPROM.h>

#define NETWORKID         212  //what network this node is on
#define NODEID              2  //this node's ID, should be unique among nodes on this NETWORKID
#define GATEWAYID           1  //central node to report data to
#define KEY "ABCDABCDABCDABCD" //(16 bytes of your choice - keep the same on all encrypted nodes)
#define DEVICENAME 1 // 1 is a switch module, 0 is a sensor module
#define EOL '#'

#define RELAY           3  //pin connected to onboard RELAY 
#define SERIAL_BAUD    9600

#define REG_ADDR 0
#define STATE_ADDR 1
#define HUBNUMBER 145 //Store two byte of data i.e use both 145 and 146

#define REQUESTACK true
#define ACK_TIME         100   
#define TXWAIT         500
#define TXWAIT         100



RFM12B radio;
char mode = 'w';      
char pinValue = '0';  

boolean registered = false;  
byte nodeId = NODEID;  
char sendBuf[7];    //The rf buffer to store outgoing data
char recvBuf[5];    // The rf buffer to store incoming data

char tempC;
int reading;
int tempPin = 0;

/* 

- Initilizes The previous state
- Checks if the device is registered with the hub

*/

void initState(){  
    analogReference(INTERNAL);
    registered=true;
    Serial.println("Not registered");
    sendMsg('R',DEVICENAME, EOL); // send a Register command [R:M] (M is the unique device name)
    recvMsg();  // listen for reply
    
    if(!registered)  {
        digitalWrite(RELAY,0); //Turn off the relay
        return;
        }

    byte eepromBuf= EEPROM.read(STATE_ADDR);
    Serial.write("Prev pin state : ");
                                           // if registered, To do: Put those information on EEPROM, send it for checksum during power on
    Serial.write(eepromBuf);
    pinValue = eepromBuf;
}

void initRadio(){
  Serial.begin(SERIAL_BAUD);
  pinMode(RELAY, OUTPUT);
  Serial.print("Initializing Radio..");
  radio.Initialize(NODEID, RF12_433MHZ, NETWORKID);
  radio.Encrypt((uint8_t*)KEY);
  Serial.println("Listening for requests...\n");
  Serial.println();
}


void setup(void)
{
 initRadio();
 delay(2000);
 initState();
 Serial.println("[Success]");
}



void loop()
{  
  if(registered)
      digitalWrite(RELAY,pinValue-'0');

      recvMsg();
      if (recvBuf[0]=='w')
      {
          pinValue = recvBuf[2];
          EEPROM.write(STATE_ADDR,pinValue);
          recvBuf[0]=' '; //Clear the buffer
          }
   
    else if(recvBuf[0]=='R') // [R:N] where N is the reply message, N can be -1,0,1 
    {
        if(recvBuf[2]== '0'){
            registered=true;
            Serial.println("The device is already registered");
        }
        else if(recvBuf[2] == '1'){
            Serial.println("The device is registered now");
            }
        else
            registered=false;
        
        recvBuf[0]=' '; // Clear the buffer
 }
 else if(recvBuf[0] == 'r'){
     reading = analogRead(tempPin);
     tempC =char(reading / 9.31);
     Serial.println(tempC);
     sendMsg('T',tempC,'#');
     recvBuf[0]=' ';
     }
    
}


void sendMsg(char param1,char param2,char ender)
{
  boolean success = false;

  long now = millis();
   sprintf(sendBuf, "%c%d%c", param1,param2,ender);
   Serial.println(sendBuf);
//  while(((millis()-now)<TXWAIT) && success==false){
  while(success==false){
  radio.Send(GATEWAYID, sendBuf, strlen(sendBuf), REQUESTACK);
  Serial.print("Request sent to node : ");
  Serial.println(GATEWAYID);
  if(waitForAck()){
    Serial.println("ACK ok");
    success=true;
  }
  else
    Serial.println("NO ACk reply");
  }
 }
 
void recvMsg(void){
if (radio.ReceiveComplete())
  {
    if (radio.CRCPass())
     {
       byte i;
       char senderNodeId = radio.GetSender();
       Serial.print('[');Serial.print(senderNodeId);Serial.print("] ");
      Serial.print("Recieved: ");
      
      for (i = 0; i < *radio.DataLen; i++){ 
        Serial.print((char)radio.Data[i]);
        recvBuf[i]=radio.Data[i];
      }
       if(senderNodeId==GATEWAYID){    // Only process The data sent from GATEWAYID
      recvBuf[i] = '\0';
       if (radio.ACKRequested())
      {
        radio.SendACK();
        Serial.print(" - ACK sent");
      }
       }
      else   // If the data is not from the GATEWAy, retransmit it
          {
        radio.Send(senderNodeId, recvBuf, strlen(recvBuf), REQUESTACK);
        Serial.print("Request sent to node : ");
        Serial.println(senderNodeId);
        if(waitForAck()){
        Serial.println("ACK ok");
        }
  else
    Serial.println("NO ACk reply");
  }
          
       
       }
    else
      Serial.print("BAD-CRC");   
      Serial.println();
  }
  else 
    return;
    }




static bool waitForAck() {
  long now = millis();
  while (millis() - now <= ACK_TIME)
    if (radio.ACKReceived(GATEWAYID))
      return true;
  return false;
}
 
